#[macro_use]
extern crate aspen;

use aspen::{BehaviorTree, Status};
use std::sync::{Arc, Mutex};
use std::{thread, time};

#[derive(Debug, Default)]
struct WorldState {
	p1: u32,
	ac: u32,
}

impl WorldState {
	pub fn set_p1(&mut self, p: u32) {
		self.p1 += p;
	}

	pub fn say_hello(&self) {
		println!("hello , p1: {}", self.p1);
	}

	pub fn say_something(&self, s: String) {
		println!("hello , p1: {}", s);
	}
}

// Entry point of the program
fn main() {
	// The sync elements are required because the Action node works in a
	// separate thread. Otherwise should not be necessary.
	let mut world_state: Arc<Mutex<WorldState>> = Default::default();

	// Create the tree - sleep to simulate work
	let root = Sequence! {
		// Addition node
		Action!{ do_add },

		Repeat!{ 2,
			Sequence!{
				Sequence! {
					Condition! { |w: &Arc<Mutex<WorldState>> | {
						let mut w = w.lock().unwrap();
						w.ac = w.ac.saturating_add(1u32);
						w.ac % 2u32 == 1u32
					}},
					Action!{ do_cmp },
					Repeat!{ 2, Action!{ |w: Arc<Mutex<WorldState>>| {
						w.lock().unwrap().say_something(String::from("----------- \n \n speak sequence 1 \n")); Status::Succeeded
					}}}
				},
				Sequence! {
					Condition! { |w: &Arc<Mutex<WorldState>> | {
						let mut w = w.lock().unwrap();
						w.ac = w.ac.saturating_add(1u32);
						w.ac % 2u32 == 0u32
					}},
					Action!{ do_cmp },
					Repeat!{ 2, Action!{ |w: Arc<Mutex<WorldState>>| {
						w.lock().unwrap().say_something(String::from("========== \n speak sequence 2 \n")); Status::Succeeded
					}}}
				}
			}
		},

		// Condition node to check if we can safely do the subtraction
		//Condition! { |_| { true }},
		Condition! { |_w: &Arc<Mutex<WorldState>>| { true }},

		Action!{ |w: Arc<Mutex<WorldState>>| {
			w.lock().unwrap().say_something(String::from("say something after contidion")); Status::Succeeded }
		},
		// Subtraction node. Only runs if the condition is successful. This one
		// doesn't do a long task (there is not sleep statement), so we can use
		// a `InlineAction` node, which will not boot up a new thread.
		InlineAction!{ |_| { println!("finished action tree"); Status::Succeeded } }
	};

	// Put it all in a tree, print it, and run it
	let mut tree = BehaviorTree::new(root);
	println!("{}", tree);
	let res = tree.run(4.0, &mut world_state, Some(hook));

	println!("\nTree finished: {:?}", res);
	println!("{:?}", world_state);
}

fn do_add(state: Arc<Mutex<WorldState>>) -> Status {
	let mut locked_state = state.lock().unwrap();

	// Sleep to simulate doing a lot of work
	thread::sleep(time::Duration::from_secs(1));
	locked_state.set_p1(6);

	Status::Succeeded
}

fn do_cmp(state: Arc<Mutex<WorldState>>) -> Status {
	let mut locked_state = state.lock().unwrap();

	// Sleep to simulate doing a lot of work
	thread::sleep(time::Duration::from_secs(1));
	if locked_state.p1 > 2 {
		Status::Succeeded
	} else {
		locked_state.set_p1(99);
		Status::Failed
	}
}

// Display the tree after each tick
fn hook<S>(tree: &BehaviorTree<S>) {
	println!("{}", tree);
}
